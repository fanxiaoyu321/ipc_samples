# 编译说明

1. 将samples文件夹拷贝到OpenHarmony源代码的/foundation/communication/ipc目录下；
2. 在/foundation/communication/ipc/BUILD.gn中添加示例代码的编译入口；

```c
group("ipc_components") {
  if (os_level == "standard") {
    deps = [
      "//foundation/communication/ipc/interfaces/innerkits/ipc_core:ipc_core",
      "//foundation/communication/ipc/interfaces/innerkits/ipc_single:ipc_single",
      "//foundation/communication/ipc/interfaces/innerkits/libdbinder:libdbinder",
      "//foundation/communication/ipc/samples:ipc_samples",   <===增加行
    ]
  } else {
    deps = [ "//foundation/communication/ipc/interfaces/innerkits/c:rpc" ]
  }
}
```

3. 执行编译命令（单独编译ipc可以使用--build-target ipc_test）；

4. 编译结果会输出在out目录下，具体进入到产品目录后的相对路径为"communication/ipc"，编译生成两个可执行文件：IpcHelloClient代表通信的客户端，IpcHelloService代表通信的服务端；

# 执行说明

1. 将编译生成的两个可执行文件push到设备的/system/bin下，并赋予可执行权限；
2. 在一个独立的控制台执行IpcHelloService，启动服务端；

```c
# IpcHelloService
[IpcHelloService]Instantiate: register to saMgr result: 0

```
3. 在另外一个独立的控制台执行IpcHelloClient启动客户端；

```c
# IpcHelloClient
[IpcHelloClient]ConnectService: connect ipc hello service success
[IpcHelloClient]IpcHelloProxy::Hello: ipc hello reply msg: Hello IPC

```
