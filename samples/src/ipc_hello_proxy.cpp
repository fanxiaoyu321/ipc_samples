/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ipc_hello_proxy.h"

#include <message_parcel.h>
#include <message_option.h>

namespace OHOS {

IpcHelloProxy::IpcHelloProxy(const sptr<IRemoteObject> &impl)
    : IRemoteProxy<IIpcHello>(impl)
{
}

void IpcHelloProxy::Hello(std::string &msg)
{
    MessageOption option;
    MessageParcel data, reply;

    data.WriteString(msg);
    int result = Remote()->SendRequest(IIpcHello::HELLO_TRANSACTION, data, reply, option);
    if (result != 0) {
        printf("[IpcHelloClient]%s: send request failed: %d", __func__, result);
        return;
    }
    std::string replyMsg;
    if (!reply.ReadString(replyMsg)) {
        printf("[IpcHelloClient]%s: read ipc hello reply msg failed", __func__);
        return;
    }
    printf("[IpcHelloClient]%s: ipc hello reply msg: %s", __func__, replyMsg.c_str());
}
} // namespace OHOS
