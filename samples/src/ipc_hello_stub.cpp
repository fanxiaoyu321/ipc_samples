/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 #include "ipc_hello_stub.h"

namespace OHOS {
class IpcHelloStub : public IRemoteStub<IIpcHello> {
public:
    int OnRemoteRequest(uint32_t code,
        MessageParcel &data, MessageParcel &reply, MessageOption &option) override;
private:
    int Hello(MessageParcel &data, MessageParcel &reply);
};

int IpcHelloStub::OnRemoteRequest(uint32_t code,
        MessageParcel &data, MessageParcel &reply, MessageOption &option)
{
    (void)option;
    if (code != IIpcHello::HELLO_TRANSACTION) {
        printf("[IpcHelloService]%s: code not expected: %d\n", __func__, code);
        return -1;
    }
    return Hello(data, reply);
}

int IpcHelloStub::Hello(MessageParcel &data, MessageParcel &reply)
{
    std::string msg;
    if (!data.ReadString(msg)) {
        printf("[IpcHelloService]%s: read hello string failed\n", __func__);
        return -1;
    }
    printf("[IpcHelloService]%s: recv hello string: %s\n", __func__, msg.c_str());
    reply.WriteString("Hello IPC");
    return 0;
}
} // namespace OHOS