/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <if_system_ability_manager.h>
#include <ipc_skeleton.h>
#include <iservice_registry.h>
#include <system_ability_definition.h>

#include "ipc_hello_stub.h"

using namespace OHOS;

sptr<IRemoteObject> g_ipcHelloService = nullptr;

void Instantiate()
{
    auto saMgr = SystemAbilityManagerClient::GetInstance().GetSystemAbilityManager();
    if (saMgr == nullptr) {
        printf("[IpcHelloService]%s: fail to get saMgr", __func__);
        return;
    }
    g_ipcHelloService = new IpcHelloStub();
    int result = saMgr->AddSystemAbility(IPC_TEST_SERVICE, g_ipcHelloService);
    printf("[IpcHelloService]%s: register to saMgr result: %d", __func__, result);
}

int main(int argc __attribute__((unused)), char **argv __attribute__((unused)))
{
    Instantiate();
    IPCSkeleton::JoinWorkThread();
}
