/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_INTERFACES_IPC_HELLO_H
#define OHOS_INTERFACES_IPC_HELLO_H

#include <iremote_broker.h>

#include <string>

namespace OHOS {

class IIpcHello : public IRemoteBroker {
public:
    enum {
        HELLO_TRANSACTION = 1,
    };
public:
    virtual void Hello(std::string &msg) = 0;
public:
    DECLARE_INTERFACE_DESCRIPTOR(u"ipc.hello.IIpcHello");
};

class TestDeathRecipient : public IRemoteObject::DeathRecipient {
public:
    virtual void OnRemoteDied(const wptr<IRemoteObject> &remote);
    TestDeathRecipient();
    virtual ~TestDeathRecipient();
    static bool GotDeathRecipient();
    static bool gotDeathRecipient_;
};
} // namespace OHOS
#endif // OHOS_INTERFACES_IPC_HELLO_H
